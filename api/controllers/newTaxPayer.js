const taxPayer = require('../models/taxPayers')
const xmljs = require('xml2js')

module.exports = async (req, res, next) => {
    taxPayer.findById(req.body["tax-filing"].pan)
        .then(response => {
            if (response) {
                res.status(200).json({
                    error: "Taxpayer is already present"
                })
            } else {                
                const id = req.body["tax-filing"].pan
                const builder = new xmljs.Builder()
                const xmlResponse = builder.buildObject(req.body)
                const data = Buffer.from(xmlResponse).toString('base64')

                const newData = new taxPayer({ "_id": id, "data": data })
                newData.save()
                    .then((result) => {
                        res.status(201).json({
                            "msg": "Saved Successfully"
                        })
                    }).catch((err) => {
                        res.status(500).json({
                            "error": err
                        })
                    });
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}