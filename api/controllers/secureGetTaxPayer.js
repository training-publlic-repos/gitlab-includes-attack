const taxPayer = require("../models/taxPayers");
const libxml = require('libxmljs')

module.exports = async (req, res, next) => {
    taxPayer.findById(req.query.pan)
        .then(response => {
            if (!response) {
                res.status(400).json({
                    error: "no such user exists"
                })
            } else {
                const x = response.data
                let xmlObject = Buffer.from(x, 'base64').toString()

                const xmlDoc = libxml.parseXml(xmlObject)

                const result = {
                    "tax-filing": {
                        "name": xmlDoc.get('//tax-filing').get('//name').text(),
                        "email": xmlDoc.get('//tax-filing').get('//email').text(),
                        "pan": xmlDoc.get('//tax-filing').get('//pan').text(),
                        "investments": {
                            "ppf": xmlDoc.get('//tax-filing').get('//investments').get('//ppf').text(),
                            "mutual-funds": xmlDoc.get('//tax-filing').get('//investments').get('//mutual-funds').text(),
                            "fixed-deposit": xmlDoc.get('//tax-filing').get('//investments').get('//fixed-deposit').text(),
                            "home-loan-principal": xmlDoc.get('//tax-filing').get('//investments').get('//home-loan-principal').text(),
                            "insurance": xmlDoc.get('//tax-filing').get('//investments').get('//insurance').text(),
                            "medical-insurance": xmlDoc.get('//tax-filing').get('//investments').get('//medical-insurance').text()
                        }
                    }

                }

                res.status(200).json(result)
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
}
