const taxPayer = require('../models/taxPayers')
const xmljs = require("xml2js")
const { Base64 } = require('js-base64')

module.exports = async (req, res, next) => {

    taxPayer.findById(req.body["tax-filing"].pan)
        .then(result => {
            if (!result) {
                res.status(200).json({
                    msg: "Please enter a valid PAN"
                })
            } else {
                const id = req.body["tax-filing"].pan
                const builder = new xmljs.Builder()
                const xmlResponse = builder.buildObject(req.body)
                const data = Buffer.from(xmlResponse).toString('base64')

                taxPayer.findByIdAndUpdate(id, { "data": data })
                    .then(response => {
                        res.status(201).json({
                            "msg": "Data Updated Successfully"
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            error: err
                        })
                    })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}
