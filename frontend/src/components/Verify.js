import { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'
import axios from 'axios'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'

function Verify() {
    const [data, setData] = useState({})
    const [dataPresent, setDataPresent] = useState(false)
    const [pan, setPan] = useState('')
    const verify = event => {
        event.preventDefault()
        axios.get(`http://localhost:8000/taxPayer/?pan=${event.target.pan.value}`)
            .then(result => {
                setPan(event.target.pan.value)
                setData(result.data)
                setDataPresent(true)
            })

    }
    const exp = event => {
        axios({
            url: `http://localhost:8000/download/?pan=${pan}`,
            method: 'GET',
            responseType: 'blob', // important
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'data.xml');
            document.body.appendChild(link);
            link.click();
        });

    }

    function getTable() {
        if (dataPresent) {
            return (<>

                    <Table striped hover>
                    <thead>
                        <tr align='center'>
                            <th rowSpan={2}>Name</th>
                            <th rowSpan={2}>PAN</th>
                            <th rowSpan={2}>Email</th>
                            <th colSpan={6}>Investments</th>
                        </tr>
                        <tr align='center'>
                            <th>Insurance</th>
                            <th>PPF</th>
                            <th>Mutual Funds</th>
                            <th>Home Loan Principal</th>
                            <th>Fixed Deposit</th>
                            <th>Medical Insurance</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr align='center'>
                            <td>{data['tax-filing'].name}</td>
                            <td>{data['tax-filing'].pan}</td>
                            <td>{data['tax-filing'].email}</td>
                            <td>{data['tax-filing'].investments.insurance}</td>
                            <td>{data['tax-filing'].investments.ppf}</td>
                            <td>{data['tax-filing'].investments['mutual-funds']}</td>
                            <td>{data['tax-filing'].investments['home-loan-principal']}</td>
                            <td>{data['tax-filing'].investments['fixed-deposit']}</td>
                            <td>{data['tax-filing'].investments['medical-insurance']}</td>
                        </tr>
                    </tbody>
                </Table>
                <Button onClick={exp}>Export to XML</Button>
            </>
            )
        } else {
            return null
        }
    }

    return (
        <>
            <Navbar expand="lg" bg="primary" variant="dark">
                <Container style={{ marginLeft: "5px" }}>
                    <Navbar.Brand>Tax-filing</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/insecure">Register New User</Nav.Link>
                        <Nav.Link href="/insecure/verify">Verify Investments</Nav.Link>
                        <Nav.Link href="/insecure/update">Update Investments</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
            <div className='content'>
                <h1>Verify Investments</h1>
                <Form className='form' onSubmit={verify}>
                    <div className='input'>
                        <Form.Group class='input-field' controlId="formBasicName">
                            <Form.Label>PAN</Form.Label>
                            <Form.Control type="text" name="pan" placeholder="Enter pan" />
                        </Form.Group>
                        
                    </div>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
                {getTable()}
            </div>
            
        </>
    )
}

export default Verify