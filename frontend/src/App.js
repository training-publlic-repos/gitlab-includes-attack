import './App.css';
import { Route, Routes } from 'react-router-dom'
import Secure from './components/Secure';
import Welcome from './components/Welcome';
import Insecure from './components/Insecure';
import Verify from './components/Verify';
import SecureVerify from './components/SecureVerify';
import Update from './components/Update';
import SecureUpdate from './components/SecureUpdate';

function App() {
  return (
    <>
      <main>
        <Routes>
          <Route path="/" element={<Welcome />} />
          <Route path="/insecure" element={<Insecure />} />
          <Route path="/secure/verify" element={<SecureVerify />} />
          <Route path="/secure/update" element={<SecureUpdate />} />
          <Route path="/insecure/verify" element={<Verify />} />
          <Route path="/insecure/update" element={<Update />} />
          <Route path='/secure' element={<Secure/>}/>
        </Routes>
      </main>
    </>
  );
}

export default App;
