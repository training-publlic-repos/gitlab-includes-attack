resource "aws_iam_user" "create_user" {
  name = format("appsecengineer-%s", random_string.random_name.result)
}

resource "aws_iam_access_key" "create_user_access_key" {
  user = aws_iam_user.create_user.name
}



output "IAM_user_info" {
  value = {
      accesskey = aws_iam_user.create_user.name
  }
}


resource "aws_iam_user_policy" "lb_ro" {
  name = format("gitlab-user-%s",random_string.random_name.result)
  user = "${aws_iam_user.create_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
resource "null_resource" "accesskey" {
  provisioner "local-exec" {
    command = "echo ${aws_iam_access_key.create_user_access_key.id} >> /root/gitlab-includes-attack/api/creds.txt"
  }
}

resource "null_resource" "secreaccesskey" {
    depends_on = [
      null_resource.accesskey
    ]
  provisioner "local-exec" {
    command = "echo ${aws_iam_access_key.create_user_access_key.secret} >> /root/gitlab-includes-attack/api/creds.txt"
  }
}

